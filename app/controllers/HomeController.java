package controllers;

import com.google.inject.Inject;
import play.libs.ws.*;
import play.mvc.*;
import com.fasterxml.jackson.databind.JsonNode;
import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {
    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    @Inject
    private WSClient ws;

    @Inject
    HomeController() { }

    public Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public Result webhookGet() {
        String mode = request().getQueryString("mode");
        String token = request().getQueryString("verify_token");
        String challenge = request().getQueryString("message");
        System.out.printf("\n\n\n\n %s %s %s\n\n\n", mode, token, challenge);
        if (!mode.isEmpty() && !token.isEmpty()) {
            if (mode.equals("testing") && token.equals("theTokenToAccount")) {

                System.out.print("\n\nMessage received\n\n");
                return ok("Heya! You sent the following message: " + challenge);
            } else {
                System.out.print("Message not received, sending bad request\n");
                return badRequest("Oops, something went wrong!");
            }
        } else {
            return badRequest("Missing parameters MODE or TOKEN");
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result webhookPost() {
        JsonNode incomingRequest = request().body().asJson();
        String name = incomingRequest.get("name").asText();
        System.out.printf("\n\n\n\n name in post is : %s\n\n\n", name);
        return ok(name + ", this was a POST request by you!\n");
    }
}