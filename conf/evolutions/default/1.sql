# --- First database schema

# --- !Ups

create table user(
  phone                     varchar(10) primary key not null,
  firstname                 varchar(12) not null,
  lastname					varchar(12),
  dob						date);

# --- !Downs


drop table if exists user;